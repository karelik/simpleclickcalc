# SimpleClickCalc
Small program I was using for **calculating** approximate date of end of pay per click campaign. You have **plan** of campaign e. g. **500 clicks**. You made ads and started to watch it. After **10 days**, campaign reached **120 clicks** and for **last week** it was **90**. You can add these numbers to app and it calculates date for you from today. Result depends on date of run, but it is 4 weeks from today.
~~~~
(500 - 120) / 90 * 7 = 28 (days from today)
~~~~

##Usage
Just run jar file.

![screenshot](http://karelik.wz.cz/images/108.png)

Enjoy it!