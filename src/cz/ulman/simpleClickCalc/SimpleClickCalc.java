package cz.ulman.simpleClickCalc;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ResourceBundle;


public class SimpleClickCalc extends Application {
	private Label resultLabel;
	private ResourceBundle messages = ResourceBundle.getBundle("messages");
	private Stage stage;
	private TextField totalField, clickField, dayField, planField;

	public void start(Stage stage) {
	    this.stage = stage;
        stage.setTitle(messages.getString("title"));
        stage.setResizable(false);
		Label totalLabel = new Label(messages.getString("clicks.total"));
		totalField = new TextField("0");
		totalField.setOnAction(event -> recalculate());
		Label clicksLabel = new Label(messages.getString("clicks.time"));
		clickField = new TextField("0");
		clickField.setOnAction(event -> recalculate());
		Label daysLabel = new Label(messages.getString("time.days"));
		dayField = new TextField("7");
		dayField.setOnAction(event -> recalculate());
		Label planLabel = new Label(messages.getString("clicks.plan"));
		planField = new TextField("1000");
		planField.setOnAction(event -> recalculate());
		HBox hBox = new HBox();
		resultLabel = new Label(messages.getString("not.available"));
        hBox.getChildren().addAll(totalLabel, totalField, clicksLabel, clickField, daysLabel, dayField, planLabel, planField, resultLabel);
        Group root = new Group();
        root.getChildren().add(hBox);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.sizeToScene();
        stage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

    private void recalculate() {
		try {
			int plan = Integer.parseInt(planField.getText());
			int total = Integer.parseInt(totalField.getText());
			int days = Integer.parseInt(dayField.getText());
			int clicks = Integer.parseInt(clickField.getText());
			double resultDouble = (plan - total) * ((double) days / clicks);
			if (Double.isNaN(resultDouble) || Double.isInfinite(resultDouble)) {
				resultLabel.setText(messages.getString("not.available"));
			} else {
				int resultInt = (int) resultDouble;
				resultLabel.setText(LocalDate.now().plusDays(resultInt).format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)));
			}
		} catch (Exception e){
			resultLabel.setText(messages.getString("not.available"));
		}
        stage.sizeToScene();
    }
}
